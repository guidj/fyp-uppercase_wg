#!/usr/bin/python

"""
Project "Thermal-Aware Grid" (TAG)
High Performance Computing Services Center
Universiti Teknologi PETRONAS

File: uppercase_wg.py
Purpose:
This script generates input files and workunits for the TestApp@Home, uppercase testing application for the TAG project.

Author: Guilherme Dinis Jr.
Last Revision: February 5, 2014
"""

import subprocess
import datetime
import os
from interval import repeat

nresultsPerHour = 10
nresultsPerWorkunit = 2
frequency = nresultsPerHour / nresultsPerWorkunit
index = 5

def Initialize():
    pass
"""
./bin/create_work -appname uppercase -wu_name wu_uppercase_1_2_01 -wu_template ./templates/wu_uppercase_1_2.xml -result_template
./templates/re_uppercase_1_2.xml -min_quorum 2 -target_nresults 2 uppercase_text_1_2_01.txt
"""

def CreateInputFile(filename):

    baseDirectory = "download"
    fullPath = baseDirectory + "/" + filename
    
    f = open(fullPath, 'w')
    
    for i in range(0, 500):
        for c in range(97, 123):
            f.write(chr(c))
            
    f.close()
    return

def GenerateWorkunit():
  
    global index
    logFileName = "uppercase_wg.log"
    errorFileName = "uppercase_wg.err"
    
    date = datetime.datetime.now()

    appname = "uppercase"
    wu_name = "wu_uppercase_1_2_" + str(index)
    wu_template = "./templates/wu_uppercase_1_2.xml"
    result_template = "./templates/re_uppercase_1_2.xml"
    min_quorum = str(2)
    target_nresults = str(2);
    inputFileName = "uppercase_text_1_2_" + str(index) + ".txt"
    index += 1
       
    CreateInputFile(inputFileName)
    
    cmd = "./bin/create_work " + " -appname " + appname + " -wu_name " + wu_name + " -wu_template " + wu_template + " -result_template " + result_template + " -min_quorum " + min_quorum + " -target_nresults " + target_nresults + " " + inputFileName
    
    p = subprocess.Popen(["./bin/create_work", "-appname", appname, 
                          "-wu_name", wu_name,
                          "-wu_template",wu_template,
                          "-result_template", result_template,
                          "-min_quorum", min_quorum,
                          "-target_nresults", target_nresults, 
                          inputFileName], stdout=subprocess.PIPE, shell=False)

    output, err = p.communicate()

    #print "Index: ", index       
    #print "error : ", err
    #print "output: ", output
    #print "return: ", p.returncode

    logFile = open(logFileName,'a')
    errFile = open(errorFileName, 'a')
            
    if (p.returncode == 1):
        #log error
        errFile.write("[" + str(date.year) + "-" + str(date.month) + "-" + str(date.day) + " " + str(date.hour) + ":" + str(date.minute) + "]\t" + output + "\n")
        
    elif (p.returncode == 0):
        #log work unit creation
        #format: [year month day hour minute second] command
        
        log = "[" + str(date.day) + " " + str(date.month) + " " + str(date.year) + " " +  str(date.hour) + " " + str(date.minute) + "] " + cmd + "\n"
                
        logFile.write(log)
        print log
        
    logFile.close()
    errFile.close()
    
    return

Initialize()

#run TempRead everty 2 seconds, forever
repeat(2, GenerateWorkunit)
#TempRead()
